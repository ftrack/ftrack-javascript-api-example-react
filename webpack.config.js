var path = require('path');
var webpack = require('webpack');

var srcPath = path.join(__dirname, 'source');
var babelLoaderIncludePaths = new RegExp(
    `(${[srcPath, 'ftrack-javascript-api'].join('|')})`
)

module.exports = {
    entry: ['babel-polyfill', path.join(srcPath, 'index.js')],
    output: {
        path: __dirname,
        filename: 'bundle.js',
    }, 
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    presets: [
                        require.resolve('babel-preset-es2015'),
                        require.resolve('babel-preset-react'),
                    ]
                }
            }, {
                test: /\.css$/,
                loader: "style-loader!css-loader",
            },
        ]
    },
};
