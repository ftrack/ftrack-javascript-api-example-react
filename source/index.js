import React from 'react';
import ReactDOM from 'react-dom';
import Hello from './component/notes';

import ftrackWidget from 'ftrack-web-widget';
import { Session } from 'ftrack-javascript-api';

export let session = null;

function onWidgetLoad() {
    var credentials = ftrackWidget.getCredentials();
    session = new Session(
        credentials.serverUrl,
        credentials.apiUser,
        credentials.apiKey
    );

    console.debug('Initializing API session.');
    session.initializing.then(function () {
        console.debug('Session initialized');
    });

    onWidgetUpdate();
}

function onWidgetUpdate() {
    var entity = ftrackWidget.getEntity();
    var event = new CustomEvent('ftrackWidgetUpdate', { detail: { entity } });
    window.dispatchEvent(event);
}

/** Initialize widget once DOM has loaded. */
function onDomContentLoaded() {
    console.debug('DOM content loaded, initializing widget.');
    ftrackWidget.initialize({
        onWidgetLoad,
        onWidgetUpdate,
    });

    ReactDOM.render(
        <Hello/>,
        document.getElementById('widget')
    );
}

window.addEventListener('DOMContentLoaded', onDomContentLoaded);
