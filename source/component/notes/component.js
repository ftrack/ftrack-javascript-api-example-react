import React from 'react';

import { session } from '../../index.js';
import './style.css';

function NoteListItem(props) {
  const note = props.note;
  return (
    <li {...props} className="note-list-item">
      <h4>{note.author.first_name} {note.author.last_name} @ {note.date.format('LLL')}</h4>
      <p>{note.content.substring(0, 20)}...</p>
    </li>
  );
}

function NoteDetailsItem({ note }) {
  if (!note) {
    return <p>No note selected.</p>;
  }
  return (
      <div className="note-details-item">
        <h4>{note.author.first_name} {note.author.last_name} @ {note.date.format('LLL')}</h4>
        <div>{note.content}</div>
        {note.replies && note.replies.length ? note.replies.map((reply) => <NoteDetailsItem note={reply} />) : null}
      </div>
  );
}

class NotesWidget extends React.Component {
  constructor() {
    super();
    this.state = { entity: {}, notes: [], selectedNote: null };
    this.onFtrackWidgetUpdate = this.onFtrackWidgetUpdate.bind(this);
  }

  onFtrackWidgetUpdate(event) {
    const entity = event.detail.entity;
    console.log('onFtrackWidgetUpdate');
    this.setState({ entity, selectedNote: null });
    const select = [
      'author.first_name', 'author.last_name', 'date', 'content'
    ];

    // Add same attributes but with replies prefix to load the same data on
    // replies.
    select.push(
        ...select.map(
            attribute => `replies.${attribute}`
        )
    )

    session.query(`
      select ${select.join(',')} from Note
      where parent_id is "${entity.id}"
      and not in_reply_to has ()
      order by thread_activity desc
    `
    ).then(
      (response) => {
        this.setState({ notes: response.data });
      }
    )
  }
  
  componentDidMount() {
    window.addEventListener('ftrackWidgetUpdate', this.onFtrackWidgetUpdate);
  }

  componentWillUnmount() {
    window.removeEventListener('ftrackWidgetUpdate', this.onFtrackWidgetUpdate);
  }

  selectNote(note) {
    console.info('Note selected', note);
    this.setState({ selectedNote: note });
  }

  render() {
    return (
      <main>
        <h1>Notes</h1>
        <ul className="notes-list">
          {this.state.notes.map((note) => <NoteListItem note={note} onClick={this.selectNote.bind(this, note)} />)}
        </ul>
        <article className="notes-detail">
          <NoteDetailsItem note={this.state.selectedNote} />
        </article>
      </main>
    )
  }

}

export default NotesWidget;
