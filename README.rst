
*********************************************
ftrack JavaScript API: React example
*********************************************

This example shows how the JavaScript API can be used in conjunction with
ES2015, React and Webpack to build a widget showing notes.

For more information on the APIs used, please refer to the documentation:

* `Building dashboard widgets <http://ftrack.rtd.ftrack.com/en/stable/developing/building_dashboard_widgets.html>`_
* `JavaScript API client <http://ftrack-javascript-api.rtd.ftrack.com/en/stable/>`_